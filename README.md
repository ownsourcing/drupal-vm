# Simple Drupal Development VM

**pre-loaded with Drupal 8**

This project provides a relatively quick and reliable, basic Drupal 8 development environment. It aims for ease of use by all members of a Drupal shop.

It should take 5-10 minutes to build or rebuild the VM from scratch on a decent broadband connection.

## Quick Start Guide

### 1 - Install dependencies (VirtualBox and Vagrant)

  1. Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
  2. Download and install [Vagrant](http://www.vagrantup.com/downloads.html).

Note on host platforms: *This machine should run equally well on Mac and PC; please file issues on the [OwnSourcing Bitbucket account](https://bitbucket.org/ownsourcing/drupal-vm/issues).*

### 2 - Build the Virtual Machine

  1. Download this project and put it wherever you want.
  2. Open Terminal, cd to this directory (containing the `Vagrantfile` and this REAMDE file).
  3. Type in `vagrant up`, and let Vagrant do its magic.

Note: *If there are any errors during the course of running `vagrant up`, and it drops you back to your command prompt, just run `vagrant provision` to continue building the VM from where you left off. If there are still errors after doing this a few times, post an issue to this project's issue queue on [Bitbucket](https://bitbucket.org/ownsourcing/drupal-vm/issues) with the error.*

### 3 - Install Drupal.

  1. Open a browser in your host environment (e.g. Chrome on your Mac/PC). In the addressbar enter [localhost:8080](http://localhost:8080); you should see the d8 installation screen
  2. Follow on-screen instructions using the following specifics
    * Give a database name of your choice
    * Database username is root
    * Leave database password blank

## Notes

  - To shut down the virtual machine, enter `vagrant halt` in the Terminal in the same folder that has the `Vagrantfile`. To destroy it completely (if you want to save a little disk space, or want to rebuild it from scratch with `vagrant up` again), type in `vagrant destroy`.

## Credits

This virtual machine was started by [Stefan Freudenberg](http://definitivedrupal.org/authors/stefan-freudenberg).

Much of this readme file comes from [Jeff Geerling's Drupal Dev VM](https://github.com/geerlingguy/drupal-dev-vm), though the virtual machine he describes has substantial differences.
