#!/usr/bin/env bash
cp -r /vagrant/provision/etc/apt/* /etc/apt/
export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y upgrade
apt-get -y install \
  curl \
  git \
  libapache2-mod-php5 \
  mysql-server \
  php5-curl \
  php5-gd \
  php5-mysql \
  phpunit \
  rsync \
  ruby-sass \
  ruby-compass \
  compass-susy-plugin \
  vim-nox

cp -r /vagrant/provision/etc/* /etc/
cp -r /vagrant/provision/home/vagrant/.bashrc /home/vagrant/
chown -R vagrant:vagrant /home/vagrant

if ! pear list | grep PHP_CodeSniffer &> /dev/null ; then
	pear install PHP_CodeSniffer
fi

COMPOSER_PATH=/usr/local/bin/composer
if [ ! -f $COMPOSER_PATH ]; then
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar $COMPOSER_PATH
fi

DRUSH_DIR=/usr/local/share/drush
if [ ! -d $DRUSH_DIR ]; then
	git clone https://github.com/drush-ops/drush.git $DRUSH_DIR
	composer --working-dir=$DRUSH_DIR install
else
	git --git-dir=$DRUSH_DIR/.git --work-tree=$DRUSH_DIR pull
fi

DRUSH_LINK=/usr/local/bin/drush
if [ ! -L $DRUSH_LINK ]; then
	ln -s $DRUSH_DIR/drush $DRUSH_LINK
fi

export FILES=/var/local/drupal
if [ ! -d $FILES ]; then
	mkdir -p $FILES
	chown -R www-data:staff $FILES
	chmod -R g+w $FILES
fi

if [ ! -L /vagrant/web/sites/default/files ]; then
	ln -s $FILES /vagrant/web/sites/default/files
fi

if [ ! -d /var/lib/mysql/drupal ]; then
	mysqladmin -u root create drupal
fi

if [ ! -L /etc/apache2/mods-enabled/rewrite.load ]; then
	a2enmod rewrite
fi

if [ -L /etc/apache2/sites-enabled/000-default ]; then
	a2dissite 000-default
fi

if [ ! -L /etc/apache2/sites-enabled/drupal ]; then
	a2ensite drupal
fi

service apache2 restart
